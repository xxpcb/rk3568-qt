INCLUDEPATH += $$PWD/src

HEADERS += \
    $$PWD/src/qtwidgetbase.h \
    $$PWD/src/qtpixmapbutton.h \
    $$PWD/src/qtsliderbar.h \
    $$PWD/src/qtlistwidget.h \
    $$PWD/src/qttoolbar.h \
    $$PWD/src/qtvideowidgetsurface.h \
    $$PWD/src/qtpagelistwidget.h

SOURCES += \
    $$PWD/src/qtwidgetbase.cpp \
    $$PWD/src/qtpixmapbutton.cpp \
    $$PWD/src/qtsliderbar.cpp \
    $$PWD/src/qtlistwidget.cpp \
    $$PWD/src/qttoolbar.cpp \
    $$PWD/src/qtvideowidgetsurface.cpp \
    $$PWD/src/qtpagelistwidget.cpp

