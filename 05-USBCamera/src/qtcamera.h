﻿#ifndef QTCAMERA_H
#define QTCAMERA_H

#include <QCamera>
#include <QCameraInfo>
#include <QCameraViewfinder>
#include <QMainWindow>

class qtCamera : public QMainWindow
{
    Q_OBJECT

public:
    qtCamera();

private slots:
    void on_cameraClick();
    void on_exitClicked();

private:
    QCamera *m_camera;
    QCameraInfo m_cameraInfo;
    QCameraViewfinder *m_viewfinder;
};

#endif
