
TARGET = USBCameraSSD
TEMPLATE = app

QT += widgets multimedia multimediawidgets

SOURCES += main.cpp

include($$PWD/qcamera.pri)

LIBS+=-lopencv_core -lopencv_objdetect -lopencv_highgui -lopencv_videoio -lopencv_imgproc -lopencv_imgcodecs -lrknn_api -lOpenCL -lpthread

#temp file
DESTDIR         = $$PWD/app_bin
MOC_DIR         = $$PWD/build/qcamera
OBJECTS_DIR     = $$PWD/build/qcamera
