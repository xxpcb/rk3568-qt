QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = MusicPlayer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp

include($$PWD/musicplayer.pri)

#temp file
DESTDIR         = $$PWD/app_bin
MOC_DIR         = $$PWD/build
QRC_DIR         = $$PWD/build
OBJECTS_DIR     = $$PWD/build


