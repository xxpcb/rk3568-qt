INCLUDEPATH += $$PWD/src

HEADERS += \
    $$PWD/src/qtwidgetbase.h \
    $$PWD/src/qtpixmapbutton.h \
    $$PWD/src/qtsliderbar.h \
    $$PWD/src/qtlistwidget.h

SOURCES += \
    $$PWD/src/qtwidgetbase.cpp \
    $$PWD/src/qtpixmapbutton.cpp \
    $$PWD/src/qtsliderbar.cpp \
    $$PWD/src/qtlistwidget.cpp

